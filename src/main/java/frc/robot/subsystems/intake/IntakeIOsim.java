package frc.robot.subsystems.intake;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.system.plant.DCMotor;
import edu.wpi.first.wpilibj.simulation.FlywheelSim;

public class IntakeIOsim implements IntakeIO {
    private final FlywheelSim motor;
    private final PIDController pidController = new PIDController(1, 0, 0);
    private double velocityRotPerSecond;

    public IntakeIOsim() {
        motor = new FlywheelSim(DCMotor.getNEO(1), 1, 1);
        velocityRotPerSecond = 0.0;
    }
    @Override
    public void updateInputs(IntakeIOInputs inputs) {
        double appliedVoltage = 12 * pidController.calculate(motor.getAngularVelocityRPM()/60,velocityRotPerSecond);
        motor.setInputVoltage(appliedVoltage);
        motor.update(0.02);
        inputs.velocity = motor.getAngularVelocityRPM()/60;
        inputs.voltage = appliedVoltage;
        inputs.follower = false;
        if (inputs.velocity > 0.25) {
            inputs.haspiece = true;
        } else if (inputs.velocity <= -0.5) {
            inputs.haspiece = false;
        }
    }
    @Override
    public void setRollerVelocity(double speed) {
        this.velocityRotPerSecond = speed;
    }
    @Override
    public void setRollerVoltage(double voltage) {
        motor.setInputVoltage(voltage);
    }
}
