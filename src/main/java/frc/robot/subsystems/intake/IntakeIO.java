package frc.robot.subsystems.intake;

import org.littletonrobotics.junction.AutoLog;

import edu.wpi.first.math.geometry.Rotation2d;

public interface IntakeIO {
    @AutoLog
    public static class IntakeIOInputs {
        public double velocity = 0;
        public double voltage = 0;
        public boolean follower = false;
        public boolean haspiece = false;
        public Rotation2d pivotTheta = new Rotation2d();
        // public Rotation2d pivotSetpoint = new Rotation2d();
    }

    public default void updateInputs(IntakeIOInputs inputs) {
    }

    public default void setRollerVelocity(double speed) {
    }

    public default void setRollerVoltage(double voltage) {
    }

    public default void setPivotVoltage(double voltage) {

    }

    public default void setPivotVelocity(double speed) {
    }

    public default Rotation2d getPivotAngle() {
        return new Rotation2d();
    }

    public default void setPivotSetpoint() {}

    public default void resetEncoder() {
    }

}
