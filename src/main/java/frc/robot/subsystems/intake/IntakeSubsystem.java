// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems.intake;

import com.revrobotics.CANSparkMax;
import com.revrobotics.RelativeEncoder;

import java.util.function.Supplier;

import org.littletonrobotics.junction.Logger;

import com.revrobotics.CANSparkLowLevel.MotorType;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.RunCommand;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import edu.wpi.first.wpilibj2.command.button.Trigger;
import frc.robot.Constants.AdvantageKitConstants;
import frc.robot.reusable_components.limit_switch.LimitSwitchIO;
import frc.robot.reusable_components.limit_switch.LimitSwitchInputsAutoLogged;
import frc.robot.reusable_components.limit_switch.LimitSwitchReal;

public class IntakeSubsystem extends SubsystemBase {
  /** Creates a new armSubsystem. */

  // private CANSparkMax m_pivotMotor;
  private PIDController pid = new PIDController(0.55, 0, 0);
  private double speed = 0.25;

  private Rotation2d setpoint = new Rotation2d();
  private Rotation2d angle = new Rotation2d();
  private IntakeIO m_IntakeIO;
  private IntakeIOInputsAutoLogged intakeIOinputs = new IntakeIOInputsAutoLogged();
  private LimitSwitchIO m_Limit;
  private LimitSwitchInputsAutoLogged limitinputs = new LimitSwitchInputsAutoLogged();

  private LimitSwitchIO limitSwitchIO;

  private static IntakeSubsystem m_instance;

  public IntakeSubsystem() {    
    // new Rotation2d().fromRotations(m_pivotMotor.getEncoder().getPosition()).getDegrees()
    

    setAngle(new Rotation2d().fromDegrees(0));
    setSetpoint(new Rotation2d().fromDegrees(0));
    switch (AdvantageKitConstants.getMode()) {
            case SIM:
                m_IntakeIO = new IntakeIOsim();
                m_Limit = new LimitSwitchIO() {
                  
                };
                break;
            case REAL:
                m_IntakeIO = new IntakeIOMax();
                m_Limit = new LimitSwitchReal();
                break;
            case REPLAY:
            default:
                m_IntakeIO = new IntakeIO() {
                };
                m_Limit = new LimitSwitchIO() {
                  
                };
                break;
    }
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
    this.m_IntakeIO.updateInputs(this.intakeIOinputs);
    SmartDashboard.putNumber("encoder",this.angle.getDegrees());
    SmartDashboard.putNumber("pid",this.pid.calculate(this.angle.getDegrees()));

    // relativeAngleRadians = ;
    Logger.processInputs("intake", this.intakeIOinputs);
    m_Limit.updateInputs(this.limitinputs);
    Logger.processInputs("limitswitch", this.limitinputs);
    
    this.angle = this.m_IntakeIO.getPivotAngle();
    // this.angle = new Rotation2d().fromDegrees(this.encoder.getPosition());

    if (DriverStation.isDisabled() || m_Limit.checkIsActivated()) {
        this.m_IntakeIO.setRollerVelocity(0);
    }

    Logger.recordOutput("intake/setpoint", this.getSetpoint().getDegrees());
    Logger.recordOutput("intake/angle", this.angle.getDegrees());
  }

  public Command runPivotVelocity() {
    return new RunCommand(() -> this.setPivotSpeed(), this);
  }

  public void setPivotSpeed() {
    //double power = this.pid.calculate(this.angle.getDegrees()); // 0.0025 is really slow.
    double power = this.pid.calculate(this.m_IntakeIO.getPivotAngle().getDegrees())*0.00275; // 0.0025 is really slow.
    
    Logger.recordOutput("intake/pid", power);
    
    this.m_IntakeIO.setPivotVelocity(power);
  }

  public Command runSetSetpoint(Rotation2d angle) {
    return new InstantCommand(() -> this.setSetpoint(angle));
  }
  public boolean getswitch() {
    return m_Limit.checkIsActivated();
  }

  public void setSetpoint(Rotation2d angle) {
    this.setpoint = angle;
    this.pid.setSetpoint(angle.getDegrees());
  }

  public void setAngle(Rotation2d angle) {
    this.angle = angle;
  }

  public Rotation2d getAngle() {
    return this.angle;
  }

  public Rotation2d getSetpoint() {
    return this.setpoint;
  }

  public void resetEncoder() {
    this.m_IntakeIO.resetEncoder();
  }
  
  public void notspin() {
    m_IntakeIO.setRollerVelocity(0);
  }

  public void setspeed(double speed) {
    m_IntakeIO.setRollerVelocity(speed);
  }

  public Command setVelocityCommand(double speed) {
    return new InstantCommand(() -> this.m_IntakeIO.setRollerVelocity(speed));
  }

  public static IntakeSubsystem getInstance() {
    if (m_instance != null) {
      return m_instance;
    } else {
      m_instance = new IntakeSubsystem();
      return m_instance;
    }
  }

  public Rotation2d getPivotSetpoint() {
    return this.setpoint;
  }

}