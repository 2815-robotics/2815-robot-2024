package frc.robot.subsystems.intake;

import javax.swing.text.Position;

import com.revrobotics.CANSparkMax;
import com.revrobotics.RelativeEncoder;

import frc.robot.Constants.IntakeConstants;

import com.revrobotics.CANSparkBase.IdleMode;
import com.revrobotics.CANSparkLowLevel.MotorType;

import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class IntakeIOMax implements IntakeIO {
    // assuming that intake only uses 1 motor to intake piece. will probably need to
    // make seperate IO class to handle up down movement of intake.
    private final CANSparkMax motor;
    private final RelativeEncoder intakeencoder;

    private final CANSparkMax m_pivotMotor = new CANSparkMax(12, MotorType.kBrushless);
    private final RelativeEncoder pivotEncoder;

    public IntakeIOMax() {
        motor = new CANSparkMax(IntakeConstants.kIntakeCanID, MotorType.kBrushless);
        intakeencoder = motor.getEncoder();

        pivotEncoder = m_pivotMotor.getEncoder();
        pivotEncoder.setPositionConversionFactor(2.0*Math.PI / 240);
        pivotEncoder.setPosition(0);

        motor.setIdleMode(IdleMode.kBrake);
        motor.isFollower();
        motor.burnFlash();
    }

    @Override
    public void updateInputs(IntakeIOInputs inputs) {
        inputs.velocity = intakeencoder.getVelocity()/60;
        inputs.voltage = motor.getBusVoltage();
        inputs.follower = motor.isFollower();
        inputs.pivotTheta = new Rotation2d().fromRotations(this.pivotEncoder.getPosition());
    }

    @Override
    public void setRollerVelocity(double speed) {
        motor.set(speed);
    }

    @Override
    public void setRollerVoltage(double voltage) {
        motor.setVoltage(voltage);
    }

    @Override
    public Rotation2d getPivotAngle() {
        return new Rotation2d().fromRotations(this.pivotEncoder.getPosition());
    }

    @Override
    public void setPivotVelocity(double speed) {
        SmartDashboard.putNumber("PIVOT",speed);
        m_pivotMotor.set(speed);
    }

    @Override
    public void setPivotVoltage(double voltage) {
        m_pivotMotor.setVoltage(voltage);
    }

    @Override
    public void resetEncoder() {
        this.pivotEncoder.setPosition(0);
    }

}
