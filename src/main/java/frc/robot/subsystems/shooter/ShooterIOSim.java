package frc.robot.subsystems.shooter;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.system.plant.DCMotor;
import edu.wpi.first.wpilibj.simulation.FlywheelSim;

public class ShooterIOSim implements ShooterIO {
    private final FlywheelSim motor;
    private final PIDController pidController = new PIDController(1, 0, 0);
    private double velocityRotPerSecond;

    public ShooterIOSim() {
        motor = new FlywheelSim(DCMotor.getNeoVortex(1), 1, 1);
        velocityRotPerSecond = 0.0;
    }

    @Override
    public void updateInputs(ShooterIOInputs inputs) {
        double appliedVoltage = 12 * pidController.calculate(motor.getAngularVelocityRPM()/60,velocityRotPerSecond);
        motor.setInputVoltage(appliedVoltage);
        motor.update(0.02);
        inputs.rotationsPerSecond = motor.getAngularVelocityRPM()/60;
        inputs.voltage = appliedVoltage;
    }
    @Override
    public void setVelocity(double speed) {
        this.velocityRotPerSecond = speed;
    }
    @Override
    public void setVoltage(double voltage) {
        motor.setInputVoltage(voltage);
    }
}
