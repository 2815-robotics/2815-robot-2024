package frc.robot.subsystems.shooter;

import org.littletonrobotics.junction.Logger;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.ParallelCommandGroup;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants.AdvantageKitConstants;
import frc.robot.subsystems.intake.IntakeSubsystem;

public class ShooterSubsystem extends SubsystemBase {
    private ShooterIO shooterIOLeft;
    private ShooterIOInputsAutoLogged shooterIOinputsLeft = new ShooterIOInputsAutoLogged();
    private ShooterIO shooterIORight;
    private ShooterIOInputsAutoLogged shooterIOinputsRight = new ShooterIOInputsAutoLogged();
    
    private static ShooterSubsystem m_instance;
    
    public ShooterSubsystem() {
        switch (AdvantageKitConstants.getMode()) {
            case SIM:
                shooterIOLeft = new ShooterIOSim();
                shooterIORight = new ShooterIOSim();
                break;
            case REAL:
                shooterIOLeft = new ShooterIOFlex(9);
                shooterIORight = new ShooterIOFlex(10);
                break;
            case REPLAY:
            default:
                shooterIOLeft = new ShooterIO() {
                };
                shooterIORight = new ShooterIO() {
                };
                break;
        }
    }

    @Override
    public void periodic() {
        this.shooterIOLeft.updateInputs(this.shooterIOinputsLeft);
        this.shooterIORight.updateInputs(this.shooterIOinputsRight);

        Logger.processInputs("shooter/ShooterIOLeft", this.shooterIOinputsLeft);
        Logger.processInputs("shooter/ShooterIORight", this.shooterIOinputsRight);

        if (DriverStation.isDisabled()) {
            this.shooterIOLeft.setVelocity(0);
            this.shooterIORight.setVelocity(0);
        }
    }

    public Command setVelocityCommand(double speed) {
        // return new InstantCommand(() -> this.shooterIOLeft.setVelocity(speed));
        return new ParallelCommandGroup(
            new InstantCommand(() -> this.shooterIOLeft.setVelocity(speed)),
            new InstantCommand(() -> this.shooterIORight.setVelocity(speed))
        );
    }
    public static ShooterSubsystem getInstance() {
    if (m_instance != null) {
      return m_instance;
    } else {
      m_instance = new ShooterSubsystem();
      return m_instance;
    }
  }
}
