package frc.robot.subsystems.shooter;

import com.revrobotics.CANSparkFlex;
import com.revrobotics.RelativeEncoder;
import com.revrobotics.CANSparkBase.IdleMode;
import com.revrobotics.CANSparkLowLevel.MotorType;

public class ShooterIOFlex implements ShooterIO {
    private final CANSparkFlex motor;
    private final RelativeEncoder encoder;

    public ShooterIOFlex(int id) {

        motor = new CANSparkFlex(id, MotorType.kBrushless);
        if (id == 10) {
            motor.setInverted(true);
        } else {
            motor.setInverted(false);
        }

        motor.setIdleMode(IdleMode.kBrake);
        motor.burnFlash();
        encoder = motor.getEncoder();
    }

    @Override
    public void updateInputs(ShooterIOInputs inputs) {
        inputs.rotationsPerSecond = encoder.getVelocity()/60;
        inputs.voltage = motor.getBusVoltage();
    }
    @Override
    public void setVelocity(double speed) {
        motor.set(speed);
    }
    @Override
    public void setVoltage(double voltage) {
        motor.setVoltage(voltage);
    }
}
