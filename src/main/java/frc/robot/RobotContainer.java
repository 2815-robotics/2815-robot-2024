// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import org.littletonrobotics.junction.networktables.LoggedDashboardChooser;

import com.pathplanner.lib.auto.AutoBuilder;
import com.pathplanner.lib.auto.NamedCommands;
import com.pathplanner.lib.path.PathPlannerPath;

import edu.wpi.first.math.MathUtil;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.smartdashboard.Field2d;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.RunCommand;
import edu.wpi.first.wpilibj2.command.button.CommandJoystick;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;
import frc.robot.Constants.OIConstants;
import frc.robot.commands.auto_commands;
import frc.robot.subsystems.drive.DriveSubsystem;
import frc.robot.subsystems.intake.IntakeSubsystem;
import frc.robot.subsystems.shooter.ShooterSubsystem;
import frc.robot.utils.CommandCustomController;

/*
 * This class is where the bulk of the robot should be declared.  Since Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in the {@link Robot}
 * periodic methods (other than the scheduler calls).  Instead, the structure of the robot
 * (including subsystems, commands, and button mappings) should be declared here.
 */
public class RobotContainer {
    public final Field2d field = new Field2d();

    // The robot's subsystems
    public final DriveSubsystem m_robotDrive = new DriveSubsystem();
    public final ShooterSubsystem m_shooter = ShooterSubsystem.getInstance();
    public final IntakeSubsystem m_intake = IntakeSubsystem.getInstance();
    public final auto_commands m_autocommands = new auto_commands();

    final CommandCustomController m_driverController = new CommandCustomController(OIConstants.kDriverControllerPort);
    final CommandJoystick m_operatorController = new CommandJoystick(OIConstants.kOperatorControllerPort);
    public final LoggedDashboardChooser<Command> m_autoChooser;
    // final CommandCustomController m_operatorController = new
    // CommandCustomController(
    // OIConstants.kOperatorControllerPort);

    /**
     * The container for the robot. Contains subsystems, OI devices, and commands.
     */
    public RobotContainer() {
        NamedCommands.registerCommand("IntakeUp", m_autocommands.IntakeUp());
        NamedCommands.registerCommand("IntakeDown", m_autocommands.IntakeDown());
        NamedCommands.registerCommand("IntakeGrab", m_autocommands.Grab());
        NamedCommands.registerCommand("Stop_Intake", m_autocommands.StopIntake());

        NamedCommands.registerCommand("collect", m_autocommands.collect_piece());

        NamedCommands.registerCommand("SpinUp_Shoot", m_autocommands.SpinUpShoot());
        NamedCommands.registerCommand("Shoot", m_autocommands.Shoot());
        NamedCommands.registerCommand("Shooter_Spin", m_autocommands.ShooterSpinUp());
        NamedCommands.registerCommand("Stop_Shooter", m_autocommands.StopShooter());

        NamedCommands.registerCommand("Load", m_autocommands.SpinLoadup());
        NamedCommands.registerCommand("RunIntake", m_autocommands.RunIntakePivot());
        // Configure the button bindings
        configureButtonBindings();

        // Configure default commands
        m_robotDrive.setDefaultCommand(
                // The left stick controls translation of the robot.
                // Turning is controlled by the X axis of the right stick.
                new RunCommand(
                        () -> m_robotDrive.drive(
                                -m_driverController.getLeftY(),
                                -m_driverController.getLeftX(),
                                -m_driverController.getRightX(),
                                true, true),
                        m_robotDrive));
        m_autoChooser = new LoggedDashboardChooser<>("Auto Chooser", AutoBuilder.buildAutoChooser());
        m_intake.setDefaultCommand(m_intake.runPivotVelocity());
        // m_intake.setDefaultCommand(new RunCommand(m_intake::runPivotVelocity, m_intake));
        // m_autoChooser.addDefaultOption("Default Auto", getAutonomousCommand());
    }

    /**
     * Use this method to define your button->command mappings. Buttons can be
     * created by
     * instantiating a {@link edu.wpi.first.wpilibj.GenericHID} or one of its
     * subclasses ({@link
     * 
     * edu.wpi.first.wpilibj.Joystick} or {@link XboxController}), and then calling
     * passing it to a
     * {@link JoystickButton}.
     */
    private void configureButtonBindings() {
        m_driverController.rightBumper()
                .whileTrue(new RunCommand(
                        () -> m_robotDrive.setCross(),
                        m_robotDrive));

        m_operatorController.trigger().whileTrue(m_intake.setVelocityCommand(-1)).onFalse(m_intake.setVelocityCommand(0));

        m_operatorController.button(2).whileTrue(
            m_shooter.setVelocityCommand(-0.6)
        ).whileFalse(m_shooter.setVelocityCommand(0)); // spin up shooter

        // pivot controls
        m_operatorController.button(5).onTrue(
            m_intake.runSetSetpoint(new Rotation2d().fromDegrees(0))
        );
        m_operatorController.button(6).onTrue(
            m_intake.runSetSetpoint(new Rotation2d().fromDegrees(209))
        );


        m_driverController.a().whileTrue(m_intake.runPivotVelocity());

        
        // intake controls
        m_operatorController.button(3).whileTrue(
            m_intake.setVelocityCommand(0.75)).onFalse(m_intake.setVelocityCommand(0));

        // having issues with 2 xboxcontrollers. this is for if operator uses xbox:
        
        // shooting controls
        // m_operatorController.axisGreaterThan(3, 0).whileTrue(
        //     m_intake.setVelocityCommand(-1)
        // ).whileFalse(m_intake.setVelocityCommand(0)); // intake spit out piece/feed into shooter

        // m_operatorController.axisGreaterThan(4, 0).whileTrue(
        //     m_shooter.setVelocityCommand(-0.6)
        // ).whileFalse(m_shooter.setVelocityCommand(0)); // spin up shooter

        // // pivot controls
        // m_operatorController.leftBumper().onTrue(
        //     m_intake.runSetSetpoint(new Rotation2d().fromDegrees(0))
        // );
        // m_operatorController.rightBumper().onTrue(
        //     m_intake.runSetSetpoint(new Rotation2d().fromDegrees(200))
        // );

        // m_operatorController.povLeft().onTrue(
        //     m_intake.runSetSetpoint(new Rotation2d().fromDegrees(0)));
        
        // m_operatorController.povUp().onTrue(
        //     m_intake.runSetSetpoint(new Rotation2d().fromDegrees(100)));
        
        // m_operatorController.povRight().onTrue(
        //     m_intake.runSetSetpoint(new Rotation2d().fromDegrees(200)));
        
        // // intake controls
        // m_operatorController.a().whileTrue(
        //     m_intake.setVelocityCommand(0.75)).whileFalse(m_intake.setVelocityCommand(0));
    }

    public Command getAutonomousCommand() {
        // Load the path you want to follow using its name in the GUI
        PathPlannerPath path = PathPlannerPath.fromPathFile("Default Auto");

        // m_robotDrive.resetPose(path.getPreviewStartingHolonomicPose());

        // Create a path following command using AutoBuilder. This will also trigger
        // event markers.
        return AutoBuilder.followPath(path);
    }
}