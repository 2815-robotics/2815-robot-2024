package frc.robot.commands;

import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.ParallelCommandGroup;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import edu.wpi.first.wpilibj2.command.WaitUntilCommand;
import frc.robot.subsystems.intake.IntakeSubsystem;
import frc.robot.subsystems.shooter.ShooterSubsystem;

public class auto_commands {
    public final ShooterSubsystem m_shooter = ShooterSubsystem.getInstance();
    public final IntakeSubsystem m_intake = IntakeSubsystem.getInstance();
    public Command SpinUpShoot() {
        return new SequentialCommandGroup(
            m_shooter.setVelocityCommand(-0.6), // shooter is told to start spinning
            new WaitCommand(0.5),
            m_intake.setVelocityCommand(-1), // intake sends note out at full speed into shooter, shooting the note
            new WaitCommand(0.5), // wait 0.5 seconds to make sure note has exited the robot
            new ParallelCommandGroup(
                m_intake.setVelocityCommand(0),
                m_shooter.setVelocityCommand(0)
            ) // stop the shooter and intake
        );
    }

    public Command Shoot() { // a command for when the shooter has already been spun up
        return new SequentialCommandGroup(
            IntakeUp().until(() -> Math.abs(m_intake.getAngle().getDegrees()) < 0.565),
            new WaitCommand(0.5),
            m_intake.setVelocityCommand(-1), // intake sends note out at full speed into shooter, shooting the note
            new WaitCommand(0.5), // wait 0.5 seconds to make sure note has exited the robot
            new ParallelCommandGroup(
                m_intake.setVelocityCommand(0),
                m_shooter.setVelocityCommand(0)
            ) // stop the shooter and intake
        );
    }

    public Command ShooterSpinUp() {
        return m_shooter.setVelocityCommand(-0.6);
    }

    public Command SpinLoadup() {
        return new SequentialCommandGroup(
            IntakeUp(),
            ShooterSpinUp()
        );
    }

    public Command StopShooter() {
        return m_shooter.setVelocityCommand(0);
    }

    public Command Grab() {
        return m_intake.setVelocityCommand(0.90);
    }

    public Command collect_piece() {
        return new SequentialCommandGroup(
            IntakeDown(),
            new WaitCommand(0.95),
            Grab().until(() -> m_intake.getswitch())
        );
    }

    public Command RunIntakePivot() {
        return m_intake.runPivotVelocity();
    }

    public Command StopIntake() {
        return m_intake.setVelocityCommand(0);
    }
    public Command IntakeUp() {
        return m_intake.runSetSetpoint(new Rotation2d().fromDegrees(0));
    }
    public Command IntakeDown() {
        return m_intake.runSetSetpoint(new Rotation2d().fromDegrees(209));
    }
}
