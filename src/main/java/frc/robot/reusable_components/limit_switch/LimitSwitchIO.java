package frc.robot.reusable_components.limit_switch;

import org.littletonrobotics.junction.AutoLog;

import frc.robot.subsystems.intake.IntakeIO.IntakeIOInputs;

public interface LimitSwitchIO {
    @AutoLog
    public static class LimitSwitchInputs {
        public boolean isConnected = false;
        public boolean isActivated = false;
    }

    public default void updateInputs(LimitSwitchInputs inputs) {
    }

    public default boolean checkIsConnected() {
        return false;
    }    
    public default boolean checkIsActivated() {
        return false;
    }
    public default void set_switch(boolean value) {
    }
}
