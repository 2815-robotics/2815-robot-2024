package frc.robot.reusable_components.limit_switch;

import edu.wpi.first.wpilibj.DigitalInput;
import frc.robot.subsystems.intake.IntakeIO.IntakeIOInputs;

public class LimitSwitchReal implements LimitSwitchIO {
    private final DigitalInput limitswitch;
    public LimitSwitchReal() {
        limitswitch = new DigitalInput(0);
    }

    @Override
    public void updateInputs(LimitSwitchInputs inputs) {
        inputs.isConnected = true;
        inputs.isActivated = !limitswitch.get();
    }

    @Override
    public boolean checkIsConnected() {
        return true;
    }
    
    @Override 
    public boolean checkIsActivated() {
        return !limitswitch.get();
    }
}
