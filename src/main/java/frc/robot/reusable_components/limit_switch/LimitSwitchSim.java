package frc.robot.reusable_components.limit_switch;

import edu.wpi.first.wpilibj.DigitalInput;

public class LimitSwitchSim implements LimitSwitchIO {
    private boolean limitswitch;
    public LimitSwitchSim() {
        limitswitch = false;
    }

    @Override
    public void updateInputs(LimitSwitchInputs inputs) {
        inputs.isConnected = true;
        inputs.isActivated = limitswitch;
    }

    @Override
    public boolean checkIsConnected() {
        return true;
    }

    @Override 
    public boolean checkIsActivated() {
        return limitswitch;
    }
    
    @Override
    public void set_switch(boolean value) {
        limitswitch = value;
    }
}
